#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/time.h>

#include "time_util.h"

void time_init(void) { }

uint64_t timestamp_now()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    return (int64_t) tv.tv_sec * 1000000 + tv.tv_usec;
}
